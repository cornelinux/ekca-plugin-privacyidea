EKCA OTP checker plugin for privacyIDEA
=======================================

This module package implements an OTP plugin for the server component of
[EKCA](https://ekca.stroeder.com) for checking OTP values with
[privacyIDEA](https://www.privacyidea.org/) web service.

Documentation:
  * [privacyIDEA: Authentication endpoints](https://privacyidea.readthedocs.io/en/latest/modules/api/auth.html).

Status: **Not** functional yet!
