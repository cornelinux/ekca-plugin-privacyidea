# -*- coding: utf-8 -*-
"""
Test EKCA service OTP plugin classes
"""

import logging

from ekca_service.plugins.otp.base import OTPCheckFailed
from ekca_service.__about__ import OTP_PLUGIN_NAMESPACE
from ekca_service.test import OTPPluginTestCase
import responses

from ekca_service.plugins.otp.privacyidea import PrivacyIdeaOTPChecker

TEST_URL = 'https://privacyidea.example.com/'
# See https://privacyidea.readthedocs.io/en/latest/modules/api/validate.html#post--validate-check
SUCCESS_BODY = """{
   "detail": {
     "message": "matching 1 tokens",
     "serial": "PISP0000AB00",
     "type": "spass"
   },
   "id": 1,
   "jsonrpc": "2.0",
   "result": {
     "status": true,
     "value": true
   },
   "version": "privacyIDEA unknown"
 }
"""

FAIL_BODY = """{
   "id": 1,
   "jsonrpc": "2.0",
   "result": {
     "status": false,
     "error": 102
   },
   "version": "privacyIDEA unknown"
 }
"""

REJECT_BODY = """{
   "id": 1,
   "jsonrpc": "2.0",
   "result": {
     "status": true,
     "value": false
   },
   "version": "privacyIDEA unknown"
 }
"""


class TestPrivacyIdeaPlugin(OTPPluginTestCase):
    plugin_name = 'privacyidea'

    privacyidea_cfg = dict(
        OTP_CHECK_URL='https://privacyidea.example.com/',
        OTP_PRIVACYIDEA_UA='my_agent',
        OTP_PRIVACYIDEA_SSLVERIFY=True,
    )

    def test001_plugin_registration(self):
        self.assertIn(self.plugin_name, self.otp_plugins)
        self.assertEqual(self.otp_plugins[self.plugin_name], PrivacyIdeaOTPChecker)

    @responses.activate
    def test002_check_default(self):
        responses.add(responses.POST,
                      TEST_URL + "/validate/check", status=200,
                      body=SUCCESS_BODY)

        otp_checker = self.otp_plugins[self.plugin_name](self.privacyidea_cfg, logging.getLogger())
        self.assertEqual(otp_checker.check('foobar', '123456'), None)

    @responses.activate
    def test003_check_fail(self):
        responses.add(responses.POST,
                      TEST_URL + "/validate/check", status=200,
                      body=REJECT_BODY)
        otp_checker = self.otp_plugins[self.plugin_name](self.privacyidea_cfg, logging.getLogger())
        self.assertRaises(OTPCheckFailed, otp_checker.check, "user", "wrongotp")

    @responses.activate
    def test004_check_fail(self):
        responses.add(responses.POST,
                      TEST_URL + "/validate/check", status=500,
                      body=FAIL_BODY)
        otp_checker = self.otp_plugins[self.plugin_name](self.privacyidea_cfg, logging.getLogger())
        self.assertRaises(OTPCheckFailed, otp_checker.check, "missing_user", "wrongotp")


if __name__ == '__main__':
    unittest.main()
