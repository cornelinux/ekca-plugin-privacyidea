#!/bin/sh

python3 setup.py clean --all
rm -r MANIFEST .coverage dist/ekca* build/* *.egg-info .tox .eggs docs/.build/*
rm ekca_service/*.py? tests/*.py? *.py?
find -name __pycache__ | xargs -n1 -iname rm -r name
